window.addEventListener('load', function () {
    // 1,获取元素
    var arrow_l = document.querySelector('.arrow-l');
    var arrow_r = document.querySelector('.arrow-r');
    var focus = document.querySelector('.focus');
    // 鼠标经过focus 就显示隐藏左右按钮
    focus.addEventListener('mouseenter', function () {
        arrow_l.style.display = 'block';
        arrow_r.style.display = 'block';
    })
    focus.addEventListener('mouseout', function () {
        arrow_l.style.display = 'none';
        arrow_r.style.display = 'none';
    })
    // 动态生成小圆圈
    // 核心思路:小圆圈的个数要和图片张数一致
    // 所以首先得到ul里面图片的张数(图片放入小li,就是小li的个数)
    // 利用循环动态生成小圆圈
    // 创建节点 creatElement('li');
    // 插入节点 ol.appendChild(li);
    var ul = focus.querySelector('ul');
})